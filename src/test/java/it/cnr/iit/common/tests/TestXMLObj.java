package it.cnr.iit.common.tests;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import it.cnr.iit.common.utility.XMLUtility;
import oasis.names.tc.xacml.core.schema.wd_17.PolicyType;
import oasis.names.tc.xacml.core.schema.wd_17.RequestType;

public class TestXMLObj {

	private String policy = null;
	private String request = null;

	public void init() {
		initPolicyAndRequest("policy.xml", "request.xml");
	}

	private void initPolicyAndRequest(String policy, String request) {
		Path policyPath = Paths.get(TestXMLObj.class.getClassLoader().getResource(policy).getPath());
		Path requestPath = Paths.get(TestXMLObj.class.getClassLoader().getResource(request).getPath());
		try {
			this.policy = new String(Files.readAllBytes(policyPath));
			this.request = new String(Files.readAllBytes(requestPath));
		} catch (IOException e) {
			fail();
		}
	}

	@Test
	public void testValidPolicy() {
		init();
		Optional<PolicyType> optPolicyType = XMLUtility.objFromXMLOptional(PolicyType.class, policy);
		assertTrue(optPolicyType.isPresent());
		PolicyType policyType = optPolicyType.get();
		assertTrue(policyType.getPolicyId().equals("test"));
		Optional<String> newPolicy = XMLUtility.objToXMLOptional(PolicyType.class, policyType, "PolicyType",
				XMLUtility.SCHEMA);
		assertTrue(newPolicy.isPresent());
	}

	@Test
	public void testWrongPolicy() {
		init();
		Optional<PolicyType> optPolicyType = XMLUtility.objFromXMLOptional(PolicyType.class, "<policy>x>");
		assertTrue(!optPolicyType.isPresent());
	}

	@Test
	public void testValidRequest() {
		init();
		Optional<RequestType> optRequestType = XMLUtility.objFromXMLOptional(RequestType.class, request);
		assertTrue(optRequestType.isPresent());
		RequestType requestType = optRequestType.get();
		Optional<String> newRequest = XMLUtility.objToXMLOptional(RequestType.class, requestType, "RequestType",
				XMLUtility.SCHEMA);
		assertTrue(newRequest.isPresent());
	}

	@Test
	public void testWrongRequest() {
		init();
		Optional<RequestType> optRequestType = XMLUtility.objFromXMLOptional(RequestType.class, "<request>x>");
		assertTrue(!optRequestType.isPresent());
	}
}
