package it.cnr.iit.common.eventhandler.events;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.cnr.iit.common.reject.Reject;

public class RequestEvent extends Event {

	private String eventType;

	public RequestEvent() {

	}

	public RequestEvent(String eventType) {
		super();
		setEventType(eventType);
	}

	@JsonIgnore
	public String getRequestID() {
		return getAdditionalProperties().get("requestID");
	}

	public void setRequestID(String requestID) {
		getAdditionalProperties().put("requestID", requestID);
	}

	@JsonIgnore
	public String getMessageID() {
		String messageId = getAdditionalProperties().get("message-id");
		if (messageId == null) {
			messageId = getAdditionalProperties().get("messageID");
		}
		return messageId;
	}

	public void setMessageID(String messageID) {
		getAdditionalProperties().put("messageID", messageID);
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		Reject.ifNull(eventType, "null parameter");
		this.eventType = eventType;
	}

	@JsonIgnore
	public boolean isEventType(String eventType) {
		return getEventType().equals(eventType);
	}

}
