package it.cnr.iit.common.eventhandler.events;

public class ResponseEvent extends Event {

	private boolean success;

	public ResponseEvent() {
	}

	public ResponseEvent(boolean success) {
		setSuccess(success);
	}

	public ResponseEvent(boolean success, String error) {
		setSuccess(success);
		setProperty("error", error);
	}

	public boolean isSuccess() {
		return success;
	}

	private void setSuccess(boolean success) {
		this.success = success;
	}

}
