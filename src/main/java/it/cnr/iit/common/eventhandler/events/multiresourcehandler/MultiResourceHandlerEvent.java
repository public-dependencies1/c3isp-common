package it.cnr.iit.common.eventhandler.events.multiresourcehandler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.Gson;

import it.cnr.iit.common.eventhandler.events.RequestEvent;

public class MultiResourceHandlerEvent extends RequestEvent {

	// private String sessionID;

	public MultiResourceHandlerEvent() {
		super();
	}

	public MultiResourceHandlerEvent(String eventType) {
		super(eventType);
	}

//	public String getSessionID() {
//		return sessionID;
//	}
//
//	public void setSessionID(String sessionID) {
//		Reject.ifNull(sessionID, "null parameter");
//		this.sessionID = sessionID;
//	}

	public String getSessionID() {
		if (getAdditionalProperties().containsKey("sessionID")) {
			return getAdditionalProperties().get("sessionID");
		} else {
			return null;
		}
	}

	public void setSessionID(String sessionID) {
		if (sessionID != null) {
			getAdditionalProperties().put("sessionID", sessionID);
		}
	}

	@JsonIgnore
	public String[] getPolicyPaths() {
		if (getAdditionalProperties().containsKey(MultiResourceHandlerEventConstants.XACML_POLICY)) {
			String content = getAdditionalProperties().get(MultiResourceHandlerEventConstants.XACML_POLICY);
			return new Gson().fromJson(content, String[].class);
		} else if (getAdditionalProperties().containsKey(MultiResourceHandlerEventConstants.XACML_POLICYIES)) {
			String content = getAdditionalProperties().get(MultiResourceHandlerEventConstants.XACML_POLICYIES);
			return new Gson().fromJson(content, String[].class);
		}
		return new String[0];
	}

	@JsonIgnore
	public String[] getRequestPaths() {
		if (getAdditionalProperties().containsKey(MultiResourceHandlerEventConstants.XACML_REQUEST)) {
			String content = getAdditionalProperties().get(MultiResourceHandlerEventConstants.XACML_REQUEST);
			return new Gson().fromJson(content, String[].class);
		} else if (getAdditionalProperties().containsKey(MultiResourceHandlerEventConstants.XACML_REQUESTS)) {
			String content = getAdditionalProperties().get(MultiResourceHandlerEventConstants.XACML_REQUESTS);
			return new Gson().fromJson(content, String[].class);
		}
		return new String[0];
	}

}
