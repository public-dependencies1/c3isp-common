package it.cnr.iit.common.eventhandler.events.bundlemanager;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BundleManagerCreateEvent extends BundleManagerBaseEvent {

	public BundleManagerCreateEvent(String dsaId, String metadataFile, String ctiFile, String requestID) {
		super(BundleManagerEventTypes.CREATE, requestID);
		setDsaId(dsaId);
		setMetadataFile(metadataFile);
		setCtiFile(ctiFile);
	}

	public BundleManagerCreateEvent(String dsaId, String metadataFile, String ctiFile) {
		super(BundleManagerEventTypes.CREATE);
		setDsaId(dsaId);
		setMetadataFile(metadataFile);
		setCtiFile(ctiFile);
	}

	@JsonIgnore
	public String getMetadataFile() {
		return getAdditionalProperties().get(BundleManagerEventConstants.METADATA_FILE);
	}

	@JsonIgnore
	public void setMetadataFile(String metadataFile) {
		getAdditionalProperties().put(BundleManagerEventConstants.METADATA_FILE, metadataFile);
	}

	@JsonIgnore
	public String getCtiFile() {
		return getAdditionalProperties().get(BundleManagerEventConstants.CTI_FILE);
	}

	@JsonIgnore
	public void setCtiFile(String ctiFile) {
		getAdditionalProperties().put(BundleManagerEventConstants.CTI_FILE, ctiFile);
	}

	@JsonIgnore
	public String getDsaId() {
		return getAdditionalProperties().get(BundleManagerEventConstants.DSA_ID);
	}

	@JsonIgnore
	public void setDsaId(String dsaId) {
		getAdditionalProperties().put(BundleManagerEventConstants.DSA_ID, dsaId);
	}
}
