package it.cnr.iit.common.eventhandler;

import java.net.URI;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import it.cnr.iit.common.eventhandler.events.SubscribeEvent;
import it.cnr.iit.common.utility.RESTUtils;

// TODO still more complex than it needs to be
public abstract class AbstractEventHandlerSubscriber implements Runnable {

	private static final Logger LOGGER = Logger.getLogger(AbstractEventHandlerSubscriber.class.getName());

	private ExecutorService executorService;
	private ScheduledExecutorService scheduledExecutorService;

	private String[] eventTypesToSubscribe = new String[] {};
	private SubscriptionStatus subscriptionStatus;

	private boolean firstStart = true;
	private int checkTime = 300;
	private int initialDelayTime = 30;

	private LinkedBlockingQueue<String> messageQueue = new LinkedBlockingQueue<>();
	private static final String MSG_SUBSCRIBED = "SUBSCRIBED";
	private static final String MSG_STARTED = "STARTED";

	public AbstractEventHandlerSubscriber() {
		subscriptionStatus = new SubscriptionStatus();
		executorService = Executors.newSingleThreadExecutor();
		scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
	}

	public AbstractEventHandlerSubscriber(String[] events) {
		this();
		eventTypesToSubscribe = events;
	}

	public abstract String getSelfEventArrivedAPI();

	public abstract String getEventHandlerSubscribeAPI();

	public abstract String getEventHandlerSubscribersAPI();

	public void start() {
		scheduledExecutorService.scheduleAtFixedRate(this, initialDelayTime, checkTime, TimeUnit.SECONDS);
	}

	public void setScheduleRate(int initialDelayTime, int checkTime) {
		this.initialDelayTime = initialDelayTime;
		this.checkTime = checkTime;
	}

	public void subscribeAll() {
		for (String eventType : eventTypesToSubscribe) {
			subscribe(eventType);
		}
		sendMessage(MSG_STARTED);
	}

	public String[] getEventTypesToSubscribe() {
		return eventTypesToSubscribe;
	}

	public void setEventTypesToSubscribe(String[] eventTypesToSubscribe) {
		this.eventTypesToSubscribe = eventTypesToSubscribe;
	}

	public void subscribe(String eventType) {
		String url = getSelfEventArrivedAPI();
		SubscribeEvent event = new SubscribeEvent();
		event.setUrl(url);
		event.setEventType(eventType);
		RESTUtils.post(getEventHandlerSubscribeAPI(), event);
		LOGGER.log(Level.INFO, "Subscription to eventType : {0}", eventType);
	}

	@Override
	public void run() {
		if (firstStart) {
			subscribeAll();
			firstStart = false;
		}
		Future<Boolean> future = executorService.submit(this::checkSubscriptionStatus);
		if (!tryGetFromFuture(future)) {
			subscribeAll();
		} else {
			sendMessage(MSG_SUBSCRIBED);
		}
	}

	private boolean tryGetFromFuture(Future<Boolean> future) {
		try {
			boolean result = future.get(15, TimeUnit.SECONDS);
			future.cancel(true);
			return result;
		} catch (Exception e) {
			return false;
		}
	}

	private boolean checkSubscriptionStatus() {
		try {
			String message = getMessage();
			boolean hasToCheck = true;
			if (message.equals(MSG_SUBSCRIBED)) {
				hasToCheck = subscriptionStatus.hasToVerify();
			}
			if (hasToCheck) {
				return checkAllEvents();
			} else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
	}

	private boolean checkAllEvents() {
		RestTemplate restTemplate = new RestTemplate();
		for (String eventType : getEventTypesToSubscribe()) {
			URI uri = UriComponentsBuilder.fromUriString(getEventHandlerSubscribersAPI())
					.queryParam("eventType", eventType).build().toUri();
			String response = restTemplate.getForObject(uri.toString(), String.class);
			if (!response.contains(getSelfEventArrivedAPI())) {
				subscriptionStatus.wrong();
				LOGGER.log(Level.WARNING, "Not subscribed to eventType : {0}", eventType);
				return false;
			}
		}
		subscriptionStatus.correct();
		return true;
	}

	private void sendMessage(String message) {
		try {
			messageQueue.put(message);
		} catch (InterruptedException e) {
			LOGGER.log(Level.WARNING, "Interrupted! ", e);
			Thread.currentThread().interrupt();
		}
	}

	private String getMessage() {
		try {
			return messageQueue.take();
		} catch (InterruptedException e) {
			LOGGER.log(Level.WARNING, "Interrupted! ", e);
			Thread.currentThread().interrupt();
			return "";
		}
	}

	private class SubscriptionStatus {
		int correct = 0;

		protected void correct() {
			correct = (correct + 1) % 10;
		}

		protected void wrong() {
			correct = 0;
		}

		protected boolean hasToVerify() {
			if (correct <= 0) {
				return true;
			} else {
				int n = ThreadLocalRandom.current().nextInt(10);
				if (n > correct) {
					return true;
				} else {
					correct -= 1;
					return false;
				}
			}
		}
	}

}
