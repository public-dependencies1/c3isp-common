package it.cnr.iit.common.eventhandler.events.bundlemanager;

public class BundleManagerDeleteResponseEvent extends BundleManagerBaseEvent {

	public BundleManagerDeleteResponseEvent(String result, String requestID) {
		super(BundleManagerEventTypes.DELETE_RESPONSE, requestID);
		setResult(result);
	}

	public BundleManagerDeleteResponseEvent(String result) {
		super(BundleManagerEventTypes.DELETE_RESPONSE);
		setResult(result);
	}

	public String getResult() {
		return getAdditionalProperties().get(BundleManagerEventConstants.RESULT);
	}

	public void setResult(String result) {
		getAdditionalProperties().put(BundleManagerEventConstants.RESULT, result);
	}

}
