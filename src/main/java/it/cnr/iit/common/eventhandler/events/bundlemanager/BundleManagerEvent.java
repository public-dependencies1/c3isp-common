package it.cnr.iit.common.eventhandler.events.bundlemanager;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class BundleManagerEvent extends BundleManagerBaseEvent {

	public BundleManagerEvent(String eventType, String dposId, String requestID) {
		super(eventType, requestID);
		setDposId(dposId);
	}

	public BundleManagerEvent(String eventType, String dposId) {
		super(eventType);
		setDposId(dposId);
	}

	@JsonIgnore
	public String getDposId() {
		return getAdditionalProperties().get(BundleManagerEventConstants.DPOS_ID);
	}

	@JsonIgnore
	public void setDposId(String dposId) {
		getAdditionalProperties().put(BundleManagerEventConstants.DPOS_ID, dposId);
	}
}
