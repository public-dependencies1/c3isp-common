package it.cnr.iit.common.eventhandler.events.bundlemanager;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BundleManagerReadResponseEvent extends BundleManagerBaseEvent {

	public BundleManagerReadResponseEvent(String fileContent, String fileName, String payloadFormat, String requestID) {
		super(BundleManagerEventTypes.READ_RESPONSE, requestID);
		setFileName(fileName);
		setFileContent(fileContent);
		setPayloadFormat(payloadFormat);
	}

	@JsonIgnore
	public String getPayloadFormat() {
		return getAdditionalProperties().get(BundleManagerEventConstants.PAYLOAD_FORMAT);
	}

	@JsonIgnore
	public void setPayloadFormat(String payloadFormat) {
		getAdditionalProperties().put(BundleManagerEventConstants.PAYLOAD_FORMAT, payloadFormat);
	}

	@JsonIgnore
	public String getFileContent() {
		return getAdditionalProperties().get(BundleManagerEventConstants.FILE_CONTENT);
	}

	@JsonIgnore
	public void setFileContent(String fileContent) {
		getAdditionalProperties().put(BundleManagerEventConstants.FILE_CONTENT, fileContent);
	}

	@JsonIgnore
	public String getFileName() {
		return getAdditionalProperties().get(BundleManagerEventConstants.FILE_NAME);
	}

	@JsonIgnore
	public void setFileName(String fileName) {
		getAdditionalProperties().put(BundleManagerEventConstants.FILE_NAME, fileName);
	}

}
