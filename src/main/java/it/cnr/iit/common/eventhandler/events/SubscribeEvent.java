package it.cnr.iit.common.eventhandler.events;

import it.cnr.iit.common.reject.Reject;

public class SubscribeEvent extends RequestEvent {

	private String url;

	public SubscribeEvent() {

	}

	public SubscribeEvent(String eventType) {
		super(eventType);
	}

	// contstructor added for BundleManager compatibility
	public SubscribeEvent(String eventType, String url) {
		super(eventType);
		this.url = url;
	}

	public SubscribeEvent(RequestEvent event) {
		super(event.getEventType());
		setAdditionalProperties(event.getAdditionalProperties());
		Reject.ifNull(getAdditionalProperties());
		if (event.getAdditionalProperties().containsKey("url")) {
			setUrl(event.getAdditionalProperties().get("url"));
		}
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		Reject.ifNull(url, "null url");
		this.url = url;
	}

}
