package it.cnr.iit.common.eventhandler.events.multiresourcehandler;

public class MultiResourceHandlerEventConstants {

	public static final String REQUEST_ID = "requestId";
	public static final String XACML_POLICY = "xacmlPolicy";
	public static final String XACML_REQUEST = "xacmlRequest";
	public static final String XACML_POLICYIES = "xacmlPolicies";
	public static final String XACML_REQUESTS = "xacmlRequests";

	private MultiResourceHandlerEventConstants() {
	}

}
