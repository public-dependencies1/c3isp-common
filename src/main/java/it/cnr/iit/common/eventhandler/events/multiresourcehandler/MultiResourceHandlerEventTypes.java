package it.cnr.iit.common.eventhandler.events.multiresourcehandler;

public class MultiResourceHandlerEventTypes {

	public static final String TRY_ACCESS = "t";
	public static final String TRY_ACCESS_RESPONSE = "nc";
	public static final String TRY_ACCESS_MULTI = "tm";
	public static final String TRY_ACCESS_MULTI_RESPONSE = "ncm";
	public static final String START_ACCESS = "s";
	public static final String START_ACCESS_RESPONSE = "sr";
	public static final String END_ACCESS = "e";
	public static final String END_ACCESS_RESPONSE = "er";
	public static final String REVOKE_ACCESS = "r";

	private MultiResourceHandlerEventTypes() {
	}

}
