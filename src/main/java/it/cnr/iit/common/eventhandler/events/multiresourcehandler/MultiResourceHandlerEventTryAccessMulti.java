package it.cnr.iit.common.eventhandler.events.multiresourcehandler;

import java.util.UUID;

import it.cnr.iit.common.utility.JsonUtility;

public class MultiResourceHandlerEventTryAccessMulti extends MultiResourceHandlerEvent {

	public MultiResourceHandlerEventTryAccessMulti(String xacmlPolicies[], String[] xacmlRequests, String sessionId) {
		super(MultiResourceHandlerEventTypes.TRY_ACCESS_MULTI);
		JsonUtility.getJsonStringFromObject(xacmlPolicies, false)
				.ifPresent(p -> setProperty(MultiResourceHandlerEventConstants.XACML_POLICYIES, p));
		JsonUtility.getJsonStringFromObject(xacmlRequests, false)
				.ifPresent(r -> setProperty(MultiResourceHandlerEventConstants.XACML_REQUESTS, r));
		setProperty(MultiResourceHandlerEventConstants.REQUEST_ID, UUID.randomUUID().toString());
		setSessionID(sessionId);
	}

}
