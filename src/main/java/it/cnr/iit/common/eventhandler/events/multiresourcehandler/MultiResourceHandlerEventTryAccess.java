package it.cnr.iit.common.eventhandler.events.multiresourcehandler;

import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

public class MultiResourceHandlerEventTryAccess extends MultiResourceHandlerEvent {

	private static final Logger LOGGER = Logger.getLogger(MultiResourceHandlerEventTryAccess.class.getName());

	public MultiResourceHandlerEventTryAccess(String xacmlPolicy, String xacmlRequest) {
		super(MultiResourceHandlerEventTypes.TRY_ACCESS);
		Map<String, String> additionalProperties = getAdditionalProperties();

		additionalProperties.put(MultiResourceHandlerEventConstants.XACML_POLICY, xacmlPolicy);
		additionalProperties.put(MultiResourceHandlerEventConstants.XACML_REQUEST, xacmlRequest);
//		JsonUtility.getJsonStringFromObject(xacmlPolicy, false)
//				.ifPresent(p -> setProperty(MultiResourceHandlerEventConstants.XACML_POLICYIES, p));
//		JsonUtility.getJsonStringFromObject(xacmlRequest, false)
//				.ifPresent(r -> setProperty(MultiResourceHandlerEventConstants.XACML_REQUESTS, r));
		additionalProperties.put(MultiResourceHandlerEventConstants.REQUEST_ID, UUID.randomUUID().toString());
		setSessionID(UUID.randomUUID().toString());

	}

}
