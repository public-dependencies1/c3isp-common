package it.cnr.iit.common.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateMetadata {
	@JsonProperty("id")
	private String id;
	@JsonProperty("dsa_id")
	private String dsaId;
	@JsonProperty("start_time")
	private String startTime;
	@JsonProperty("end_time")
	private String endTime;
	@JsonProperty("event_type")
	private String eventType;
	@JsonProperty("organization")
	private String organization;
	@JsonProperty("subjectId")
	private String subjectId;

	public CreateMetadata() {

	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("dsa_id")
	public String getDsaId() {
		return dsaId;
	}

	@JsonProperty("dsa_id")
	public void setDsaId(String dsaId) {
		this.dsaId = dsaId;
	}

	@JsonProperty("start_time")
	public String getStartTime() {
		return startTime;
	}

	@JsonProperty("start_time")
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	@JsonProperty("end_time")
	public String getEndTime() {
		return endTime;
	}

	@JsonProperty("end_time")
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@JsonProperty("event_type")
	public String getEventType() {
		return eventType;
	}

	@JsonProperty("event_type")
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	@JsonProperty("organization")
	public String getOrganization() {
		return organization;
	}

	@JsonProperty("organization")
	public void setOrganization(String organization) {
		this.organization = organization;
	}

	@JsonProperty("subjectId")
	public String getSubjectId() {
		return subjectId;
	}

	@JsonProperty("subjectId")
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

}
