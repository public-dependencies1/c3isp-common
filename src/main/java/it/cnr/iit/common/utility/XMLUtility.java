package it.cnr.iit.common.utility;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.Optional;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

public class XMLUtility {

	private static final Logger LOGGER = Logger.getLogger(Base64Utility.class.getName());

	public static final String SCHEMA = "urn:oasis:names:tc:xacml:3.0:core:schema:wd-17";

	private XMLUtility() {
	}

	public static final <T> String objToXML(Class<T> clazz, T object, String name, String schema) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		JAXBElement<T> elem = new JAXBElement<>(new QName(schema, name), clazz, null, object);
		StringWriter stringWriter = new StringWriter();
		marshaller.marshal(elem, stringWriter);
		return stringWriter.getBuffer().toString();
	}

	public static final <T> Optional<String> objToXMLOptional(Class<T> clazz, T object, String name, String schema) {
		try {
			String str = objToXML(clazz, object, name, schema);
			return Optional.of(str);

		} catch (JAXBException e) {
			LOGGER.info("objToXML error : " + e.getMessage());
			return Optional.empty();
		}
	}

	public static final <T> T objFromXML(Class<T> clazz, String xml) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		Source stream = new StreamSource(new ByteArrayInputStream(xml.getBytes()));
		return unmarshaller.unmarshal(stream, clazz).getValue();
	}

	public static final <T> Optional<T> objFromXMLOptional(Class<T> clazz, String xml) {
		try {
			T obj = objFromXML(clazz, xml);
			return Optional.of(obj);

		} catch (JAXBException e) {
			LOGGER.info("objFromXML error : " + e.getMessage());
			return Optional.empty();
		}
	}
}
