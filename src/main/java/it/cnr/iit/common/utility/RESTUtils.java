package it.cnr.iit.common.utility;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

public final class RESTUtils {

	private static final Logger log = Logger.getLogger(RESTUtils.class.getName());

	private RESTUtils() {
	}

	public static Optional<ResponseEntity<Void>> post(String uri, Object obj) {
		return post(uri, obj, Void.class);
	}

	public static <T, E> Optional<ResponseEntity<T>> post(String uri, E obj, Class<T> responseClass) {
		Optional<String> jsonString = JsonUtility.getJsonStringFromObject(obj, false);
		if (!jsonString.isPresent()) {
			return Optional.empty();
		}

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<>(jsonString.get(), headers);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<T> responseEntity = restTemplate.postForEntity(uri, entity, responseClass); // NOSONAR

		checkResponesEntity(responseEntity);

		return Optional.of(responseEntity);
	}

	public static <E> CompletableFuture<ResponseEntity<Void>> asyncPost(String uri, E obj) {
		return asyncPost(uri, obj, Void.class);
	}

	public static <T, E> CompletableFuture<ResponseEntity<T>> asyncPost(String uri, E obj, Class<T> clazz) {
		Mono<ResponseEntity<T>> response = WebClient.create(uri).post().contentType(MediaType.APPLICATION_JSON)
				.body(BodyInserters.fromObject(obj)).exchange().flatMap(r -> r.toEntity(clazz));

		return CompletableFuture.supplyAsync(getResponseEntity(response));
	}

	private static <T> Supplier<ResponseEntity<T>> getResponseEntity(Mono<ResponseEntity<T>> response) {
		return () -> {
			ResponseEntity<T> responseEntity = response.block();
			checkResponesEntity(responseEntity);
			return responseEntity;
		};
	}

	private static <T> void checkResponesEntity(ResponseEntity<T> responseEntity) {
		if (!responseEntity.getStatusCode().is2xxSuccessful()) {
			String uri = responseEntity.getHeaders().getLocation().toString();
			log.log(Level.SEVERE, "Error posting to : {0}", uri);
		}
	}

	public static Optional<URL> parseUrl(String url) {
		try {
			return Optional.of(new URL(url));
		} catch (MalformedURLException e) {
			return Optional.empty();
		}
	}
}