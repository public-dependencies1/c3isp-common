package it.cnr.iit.common.analytic;

import java.util.concurrent.Future;

public class AnalyticManager<T> {

	private FutureMap<T> futureMap = new FutureMap<T>();

	public AnalyticManager() {
	}

	public String stopAnalytic(String sessionId) {

		Future<T> future = futureMap.getFuture(sessionId);

		if (future == null)
			return "The analytic with sessionId " + sessionId + " does not exists or has not started yet";
		if (future.isDone())
			return "The analytic with sessionId " + sessionId + " could not be stopped because it has already finished";
		if (future.isCancelled())
			return "The analytic with sessionId " + sessionId + " has been already stopped";

		future.cancel(true);
		futureMap.removeFuture(sessionId);

		return "The analytic with sessionId " + sessionId + " has been correctly stopped";

	}

	public FutureMap<T> getFutureMap() {
		return futureMap;
	}

	public void setFutureMap(FutureMap<T> futureMap) {
		this.futureMap = futureMap;
	}

}
