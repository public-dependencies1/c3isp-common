package it.cnr.iit.common.analytic;

public abstract class AbstractAnalyticExecutor {

	protected abstract <T> T executeWithResult(Object... objects);

	protected abstract <T> T executeWithResult();

	protected abstract void execute(Object... objects);

	protected abstract void execute();

}
